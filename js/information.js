 // Declare variable that contain paths to JSON directories
 var candyDataPath = "json/candy/";
 var cupcakeDataPath = "json/cupcake/";
 
 // Declare empty arrays to store information from JSON files
 var names = [];
 var prices = [];
 var images = [];
 var description=[];
 
 //Declare an empty array that will contain the full paths of JSON files read from a directory
 var wholePath = [];
 
$(document).ready(function(){
 
 // A function that fills the dropdown menu with information from .json files.  
 //Variables candyDataPath or cupcakeDataPath will be passed into this function.
 
 function fillDropDown(path) {
     $.ajax({
         url: 'getFileNames.php',        //PHP file that scans the given directory of .json files and returns a json formatted string that contains all filenames in the directory.
         type: 'POST',
         data: { jsonDir: path },
         dataType: 'json',               //Tells AJAX to expect the response to be JSON formatted
         success: function(response) {   // response is the json string containing filenames
           var filenames = [];  
           filenames = response;
           names = [];		    //Make sure the arrays are empty
           prices = [];
           images = [];
		   description=[];
          // $("#selectList").find("option:gt(0)").remove();         //Removes all options in the dropdown menu except for the first one
           $('#selectList').css('visibility','visible');  //Makes the dropdown menu visible

             $.each(filenames, function(key, value) {     
               wholePath[key]=path+value;                 //Join the directory path with the filename for each filename provided
             });  

            $.each(wholePath, function(key, value) {                    
               $.getJSON(wholePath[key], function(data, result){     //Using each built path, get the data from the json files and place the data from those files into arrays.                
                 names[key] = data.names;                 		//These arrays will be used to populate the display.
                 prices[key] = data.price;                 
                 images[key] = data.image_path;
				 description[key] = data.description;  
                 $("#selectList").append("<option value='"+key+"'>" + data.names +"</option>");  //Places the options into the dropdown menu with the array index as the value.
               });
             });  
         },
         error: function() {
           alert("Error");
         }
     });   
 }

 //A function that places the selected information into the display.  The value of the selection is passed in.
 //This value corresponds to the index value of each array and is used locate the correct information to populate the display.
     
 function loadCupcake(treatSelectionValue) {
     $('.productDescription').css('visibility','visible');
     $('#item_1').html(names[treatSelectionValue]);
     $('#item_2').html(prices[treatSelectionValue]);
     $('#item_3').html("<img src='" + images[treatSelectionValue] + "' />");
	 $('#item_4').html(description[treatSelectionValue]);
 }

 $('#candy, #cupcake').checkboxradio({
   icon: false
 });

 $('#candy').click(function() { 
       $("#cupcake").prop('checked', false);   //Makes sure the Cupcake radio button is deselected when Candy button is pressed.
       $("#selectList").html('');
       $("#selectList").html("<option>--Choose Candy--</option>");
       fillDropDown(candyDataPath);            //Calls the function that fills the dropdown menu.  Passes in the path of the directory containing the .json files for candy information.
       $('.productDescription').css('visibility','hidden');
       $('#item_1').html("");
       $('#item_2').html("");
       $('#item_3').html("");
	   $('#item_4').html("");
 });

 $('#cupcake').click(function() {  
       $("#candy").prop('checked', false);    //Makes sure the Candy radio button is deselected when Cupcake button is pressed. 
       $("#selectList").html('');
       $("#selectList").html("<option>--Choose Cupcake--</option>");
       fillDropDown(cupcakeDataPath);	 //Calls the function that fills the dropdown menu.  Passes in the path of the directory containing the .json files for cupcake information.
       $('.productDescription').css('visibility','hidden');
       $('#item_1').html("");
       $('#item_2').html("");
       $('#item_3').html("");
	   $('#item_4').html("");
 });

 $('select[name="selectTreat"]').change(function(){			//When a selection is made in the dropdown menu, the value of the selected item is stored in the ddlSelection variable.   
     var ddlSelection = $('#selectList option:selected').val();	
     loadCupcake(ddlSelection);					//Calls the function that populates the display area.  The value of the selection is passed into the function.
 });

});
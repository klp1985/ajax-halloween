<?php 
$dir = $_POST['jsonDir'];  //Get the path of the directory from the POST variable and store it in variable $dir.  

$filenames = array();  //Create empty array that will store filenames

if (is_dir($dir)){    //checks whether $dir is a directory
    if ($dh = opendir($dir)){    //Open the directory
      while (($file = readdir($dh)) !== false){  //Keep looping through the following code until no more files are found
          if($file=="." || $file=="..") {   //Ignore current and parent directory entries     
          } else {
            //echo $file . "<br>";
            array_push($filenames, $file);  //Place the found filename into the filenames array
          }      
      }
      closedir($dh);  //Close the directory
      echo json_encode($filenames);  //Converts the filenames array to a string in the JSON format. This is returned in the response variable of the AJAX call
    }
  }
?>